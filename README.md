# Yahtzee Kata

This is an implementation of the Yahtzee Refactoring Kata in LabVIEW.

I pulled this idea from https://github.com/emilybache/Yatzy-Refactoring-Kata.

## Background

Yahtzee is a dice game. It uses normal 6-sided dice. You roll 5 of them. You score points based on various categories. For example in the chance category you simply sum all the dice, for the yahtzee category you score 50 points if all the dice match or 0 if they don't. You can see the official Yahtzee Rules https://www.ultraboardgames.com/yahtzee/game-rules.php, which serve as the requirements.

## Instructions

What you'll find here is a very poorly, yet functional implementation of a scoring system. There are a set of VIs, one for each box on the scorecard, that take the 5 dice values as inputs and output a score. We know it is functional because there is a set of Unit Tests that pass. 

Your goal is to refactor this code into something presentable. You can assume that this code is not used anywhere else yet, so you can make changes without worrying about breaking anyone else's existing code. Practice taking small tiny steps and running the tests after each step. You want the tests to stay green the entire time. 

NOTE: The tests themselves could use some cleanup and you may find yourself editing the tests as you change the production code. That is ok. I intentionally left a bug in the code, so see if you can catch it. You can also feel free to add testcases or remove test cases. The ultimate end goal is clean code with good test coverage.

## Dependencies

The code is in LabVIEW 2020, but I have backsaved the code to 2014. Click on releases and you'll see a zip there. or just click here https://gitlab.com/sas-blog/yahtzee-kata/uploads/d067bfc0b78188de01780c718a2e3c37/Yahtzee_Kata_2014.zip It does require JKI VI Tester.

## TDD

You could also use this premise as a TDD kata, simply by starting from scratch.
